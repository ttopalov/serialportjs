const fs = require('fs');
const EventEmitter = require('events');
const { spawn } = require('child_process');
const exec = require('util').promisify(require('child_process').exec);
const platform = require('os').platform();

const parityValues = {
  none: 0,
  odd: 1,
  even: 2,
  mark: 3,
  space: 4
}

class Serial {
  constructor(path) {
    this.path = path;
    this._fd = null;
    this._readStream = null;
    this._writeStream = null;
    this.eventStream = new EventEmitter();
  }

  async open() {
    this._fd = null;
    this._writeStream = null;
    if (platform == 'win32') {
      this._subprocess = spawn('serialpipe.exe',
        [this.path, this.settings.baudRate, this.settings.databits || 8, this.settings.stopbits === 2 ? 2 : 0, parityValues[this.settings.parity] || 0],
        { stdio: 'pipe', windowsHide: true }
      );
      this._readStream = this._subprocess.stdout;
      this._writeStream = this._subprocess.stdin;
      this._subprocess.stderr.on('data', (data) => console.error(data.toString()));
    } else {
      this._fd = await fs.promises.open(this.path, 'rs+');
      this._writeStream = fs.createWriteStream(null, { fd: this._fd });
      this._readStream = fs.createReadStream(null, { fd: this._fd });
    }
    this._readStream.on('data', buffer => {
      this.eventStream.emit('data', buffer);
    });
  }

  async write(data) {
    if (!this._writeStream) {
      throw new Error('Can\'t write to an uninitialized port');
    }
    this._writeStream.write(Buffer.from(data));
  }

  async close() {
    if (this._fd) {
      await fs.promises.close(this._fd);
      this._fd = null;
    }
    if (this._subprocess) {
      this._subprocess.kill();
      this._subprocess = null;
    }
    this._writeStream = null;
    this._readStream = null;
  }

  async config(settings) {
    const args = [];
    switch (platform) {
      case 'linux':
      case 'darwin':
        const cmd = platform === 'linux' ? 'stty -F' : 'stty -f';
        const { stdout } = await exec(`${ cmd } ${ this.path } -a`);
        const specials = ['intr', 'quit', 'erase', 'kill', 'eof', 'eol', 'eol2', 'swtch', 'start', 'stop', 'susp', 'rprnt', 'werase', 'lnext', 'discard']
          .filter(s => stdout.indexOf(s) >= 0);
        args.push(cmd, this.path, settings.baudRate, 'raw', '-echo', '-echoe', '-echok');
        for (const special of specials) {
          args.push(special, 'undef');
        }
        if (settings.stopbits === 2) {
          args.push('cstopb');
        } else if (settings.stopbits === 1) {
          args.push('-cstopb');
        }
        if (settings.databits) {
          args.push(`cs${settings.databits}`);
        }
        if (settings.parity === 'even') {
          args.push('parenb');
          args.push('-parodd');
        } else if (settings.parity === 'odd') {
          args.push('parenb');
          args.push('parodd');
        } else {
          args.push('-parenb');
        }
        await exec(args.join(' '));
        break;
      case 'win32':
        this.settings = settings;
        break;
      default:
        throw new Error('Cannot set serial port settings on platform ' + platform);
    }
  }
}

module.exports = Serial;
