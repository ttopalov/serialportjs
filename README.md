# serialportjs

Serial port library for node using pure JS (almost). It has no dependencies and has no native components. This is why buildutils is not required during npm install.

## Usage
```js
const com = new SerialPort('/dev/ttyS1');
await com.config({
    baudRate: 38400,
    databits: 8,
    stopbits: 1,
    parity: "none"
});
await com.open();
// eventStream is a readable stream. It can be piped or just:
com.eventStream.on('data', data => {
    // data is a Buffer instance
});
com.write(Buffer.from([1, 2, 3, 4]));
```

## Linux implementation
In Linux and MacOS this library uses standard I/O operations to read/write serial ports, and calls stty command to set configuration

## Windows implementation
In Windows, the library uses the serialpipe binary which must be installed and put in the path separately
It can be found here:
https://gitlab.com/ttopalov/serialpipe
